#!/bin/sh 

set -e

# called by uscan with '--upstream-version' <version> <file>
echo "version $2"
upstream_package="spring-security"
package=`dpkg-parsechangelog | sed -n 's/^Source: //p'`
debian_version=`dpkg-parsechangelog | sed -ne 's/^Version: \(.*\)-.*/\1/p'`
TAR=${package}_${debian_version}.orig.tar.gz
DIR=${package}-${debian_version}.orig
GIT="git://git.springsource.org/spring-security/spring-security.git"

# clean up the upstream sources
unzip $3 && mv ${upstream_package}-$2 $DIR

for j in $DIR/dist/*-sources.jar; do
    unzip $j \
        -d "$DIR/dist/$(basename $j | sed 's/-[0-9\.A-Za-z]*-sources.jar$//')"
done

# include documentation sources from upstream git repo
git clone $GIT $DIR/git
cd $DIR/git && git checkout -b ${debian_version} ${debian_version} \
    && mv src/docbkx ../docs/reference && cd $OLDPWD

GZIP=--best tar --numeric --group 0 --owner 0 --anchored \
   -X debian/orig-tar.excludes -c -v -z -f $TAR $DIR

rm -rf $3 $DIR
