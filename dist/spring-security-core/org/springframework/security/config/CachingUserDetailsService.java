package org.springframework.security.config;

import org.springframework.security.providers.dao.UserCache;
import org.springframework.security.providers.dao.cache.NullUserCache;
import org.springframework.security.userdetails.UserDetailsService;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.util.Assert;

/**
 *
 * @author Luke Taylor
 * @since 2.0
 */
class CachingUserDetailsService implements UserDetailsService {
	private UserCache userCache = new NullUserCache();
	private UserDetailsService delegate;

	CachingUserDetailsService(UserDetailsService delegate) {
		this.delegate = delegate;
	}

	public UserCache getUserCache() {
		return userCache;
	}

	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}

	public UserDetails loadUserByUsername(String username) {
		UserDetails user = userCache.getUserFromCache(username);
		
		if (user == null) {
			user = delegate.loadUserByUsername(username);
		}
		
		Assert.notNull(user, "UserDetailsService " + delegate + " returned null for username " + username + ". " +
				"This is an interface contract violation");
		
		userCache.putUserInCache(user);
		
		return user;
	}
}
